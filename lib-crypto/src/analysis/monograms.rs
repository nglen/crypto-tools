/// Returns the probability that a monogram
/// chosen at random from an english text
/// is the monogram given.
#[allow(clippy::all)]
pub fn prob(c: u8) -> f64 {
    match c {
        b'A' => 0.081_67f64.ln(),
        b'B' => 0.014_92f64.ln(),
        b'C' => 0.027_82f64.ln(),
        b'D' => 0.042_53f64.ln(),
        b'E' => 0.127_02f64.ln(),
        b'F' => 0.022_28f64.ln(),
        b'G' => 0.020_15f64.ln(),
        b'H' => 0.060_94f64.ln(),
        b'I' => 0.069_66f64.ln(),
        b'J' => 0.001_53f64.ln(),
        b'K' => 0.007_72f64.ln(),
        b'L' => 0.040_25f64.ln(),
        b'M' => 0.024_06f64.ln(),
        b'N' => 0.067_49f64.ln(),
        b'O' => 0.075_07f64.ln(),
        b'P' => 0.019_29f64.ln(),
        b'Q' => 0.000_95f64.ln(),
        b'R' => 0.059_87f64.ln(),
        b'S' => 0.063_27f64.ln(),
        b'T' => 0.059_87f64.ln(),
        b'U' => 0.063_27f64.ln(),
        b'V' => 0.090_56f64.ln(),
        b'W' => 0.023_60f64.ln(),
        b'X' => 0.001_50f64.ln(),
        b'Y' => 0.019_74f64.ln(),
        b'Z' => 0.000_74f64.ln(),
        _ => unreachable!(),
    }
}
