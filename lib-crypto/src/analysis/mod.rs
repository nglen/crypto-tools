mod bigrams;
mod monograms;

pub use self::bigrams::prob as prob_bigram;
pub use self::monograms::prob as prob_monogram;
use rayon::prelude::*;
use std::collections::HashMap;
use std::hash::Hash;
use std::sync::Mutex;

/// Counts the number of times each element occurs
/// and stores the result within a [HashMap].
///
/// # Examples
///
/// ```
/// use rayon::prelude::*;
/// use lib_crypto::analysis::par_freq;
///
/// let f = par_freq("The quick brown fox jumped over the lazy dog".par_chars());
///
/// assert_eq!(f.get(&'T'), Some(&1));
/// assert_eq!(f.get(&'o'), Some(&4));
/// assert_eq!(f.get(&'O'), None);
/// ```
///
/// ['HashMap']: std::collections::HashMap
pub fn par_freq<T>(data: T) -> HashMap<T::Item, u32>
where
    T: ParallelIterator,
    T::Item: Eq + Hash + Send + Sync,
{
    let ret = Mutex::new(HashMap::new());

    data.for_each(|e| {
        *ret.lock().unwrap().entry(e).or_insert(0) += 1;
    });

    ret.into_inner().unwrap()
}

/// Counts the number of times each element occurs
/// and stores the result within a [HashMap].
///
/// # Examples
///
/// ```
/// use lib_crypto::analysis::freq;
///
/// let f = freq("The quick brown fox jumped over the lazy dog".chars());
///
/// assert_eq!(f.get(&'T'), Some(&1));
/// assert_eq!(f.get(&'o'), Some(&4));
/// assert_eq!(f.get(&'O'), None);
/// ```
///
/// ['HashMap']: std::collections::HashMap
pub fn freq<T>(data: T) -> HashMap<T::Item, u32>
where
    T: Iterator,
    T::Item: Eq + Hash,
{
    let ret = Mutex::new(HashMap::new());

    data.for_each(|e| {
        *ret.lock().unwrap().entry(e).or_insert(0) += 1;
    });

    ret.into_inner().unwrap()
}
