use rand::{rngs::OsRng, Rng};
use crate::utils::generic_mod::{Modular, Modulo};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Signer {
    generator: Modulo, // modulo p
    private: Modulo, // modulo p-1
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Verifier {
    generator: Modulo, // modulo p
    public: Modulo, // modulo p
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Signature {
    alpha: Modulo, // modulo p
    beta: Modulo, // modulo p-1
}

impl Signer {
    pub fn new(p: u32, generator: u32) -> Self {
        let mut rng = OsRng::new().expect("Failed to create OsRng");

        let generator = generator.to_modulo(p);
        let private = rng.gen_range(2, p-2).to_modulo(p-1);
        Self { generator, private }
    }

    pub fn sign(&self, message: u32) -> Signature {
        let p = self.generator.modulus();
        let mut rng = OsRng::new().expect("Failed to create OsRng");

        let message = message.to_modulo(p-1);

        // modulo p-1
        (0..).filter_map(|_| {
                let k = rng.gen_range(2, p-1).to_modulo(p-1);
                let k_inv = k.inv()?;
                Some((k, k_inv))
            }).map(|k| {
                // modulo p
                let alpha = self.generator.pow(k.0);

                // modulo p-1
                let beta = (message - self.private * alpha.to_modulo(p-1)) * k.1;

                Signature { alpha, beta }
            }).find(|sig| sig.beta.value() != 0).unwrap()
    }

    pub fn public(&self) -> Verifier {
        Verifier {
            generator: self.generator,
            public: self.generator.pow(self.private)
        }
    }

    pub fn verify(&self, message: u32, sig: Signature) -> bool {
        self.public().verify(message, sig)
    }
}

impl Verifier {
    pub fn verify(&self, message: u32, sig: Signature) -> bool {
        let lhs = self.public.pow(sig.alpha) * sig.alpha.pow(sig.beta);
        let rhs = self.generator.pow(message);
        lhs == rhs
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_sign_verify() {
        let signer = Signer::new(71, 7);
        let verifier = signer.public();
        
        for m in 0..71 {
            let sig = signer.sign(m);
            assert!(signer.verify(m, sig));
            assert!(verifier.verify(m, sig));
        }
    }
}
