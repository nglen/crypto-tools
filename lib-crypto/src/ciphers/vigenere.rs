///! Viginere Cipher
///
/// A polyalphabetic substitution cipher.
/// A vigenere cipher of length m is equivalent to
/// m interwoven shift ciphers.
use crate::analysis::freq;
use crate::ciphers::shift;
use crate::prelude::*;
use rayon::prelude::*;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Key {
    text: Vec<u8>,
}

impl Key {
    pub fn new(text: Vec<u8>) -> Self {
        Self { text }
    }

    pub fn as_str(&self) -> Result<&str, std::str::Utf8Error> {
        std::str::from_utf8(&self.text)
    }
}

impl Encrypt for Key {
    type CodePoint = u8;

    fn encrypt(&self, pt: &[u8]) -> Vec<u8> {
        let key_iter = self
            .text
            .iter()
            .cloned()
            .filter_map(M26::from_ascii)
            .cycle();
        let text_iter = pt.iter().cloned().filter_map(M26::from_ascii);
        text_iter
            .zip(key_iter)
            .map(|pair| pair.0 + pair.1)
            .map(M26::into_ascii)
            .collect()
    }
}

impl Decrypt for Key {
    type CodePoint = u8;

    fn decrypt(&self, ct: &[u8]) -> Vec<u8> {
        let key_iter = self
            .text
            .iter()
            .cloned()
            .filter_map(M26::from_ascii)
            .cycle();
        let text_iter = ct.iter().cloned().filter_map(M26::from_ascii);
        text_iter
            .zip(key_iter)
            .map(|pair| pair.0 - pair.1)
            .map(M26::into_ascii)
            .collect()
    }
}

/// Returns the key used to decrypt the vigenere cipher.
///
/// # Arguments
///
/// * 'text' - The cipher-text to decode
/// * 'max' - The max key length to try (exclusively)
pub fn crack(text: &[u8], max: usize) -> Key {
    let key_len = guess_key_len(text, max);
    Key::new(
        (0..key_len)
            .into_par_iter()
            .map(|i| {
                let sub_text: Vec<_> = text.iter().skip(i).step_by(key_len).cloned().collect();
                shift::crack(&sub_text).shift.into_ascii()
            })
            .collect(),
    )
}

/// Returns the guessed length of the key used to decrypt the vigenere cipher.
///
/// A lower max value will generally give a better result. This is because all multiples of m are
/// valid. For example, if the key is Key, then KEYKEY is also valid.
///
/// # Arguments
///
/// * 'text' - The cipher-text to find the key of
/// * 'max' - The max key length to try (exclusively)
pub fn guess_key_len(text: &[u8], max: usize) -> usize {
    let text: Vec<_> = text
        .par_iter()
        .filter(|c| c.is_ascii_alphabetic())
        .map(u8::to_ascii_uppercase)
        .collect();

    (1..max)
        .map(|step| (step, average_ioc(&text, step)))
        .fold((0, std::f64::NAN), |a, b| if a.1 > b.1 { a } else { b })
        .0
}

/// Splits the given text into a number of subsets and averages the IOCs of them all.
///
/// # Arguments
///
/// * 'text' - The text to get the average IOC of
/// * 'step' - The number of steps between each element of a subset
fn average_ioc(text: &[u8], step: usize) -> f64 {
    let len = text.len() / step;
    (0..step)
        .into_par_iter()
        .map(|i| calculate_ioc(text.iter().skip(i).step_by(step).cloned(), len))
        .sum::<f64>()
        / step as f64
}

/// Calculates the index of coincidence (IOC) of a given text
///
/// # Arguments
///
/// * 'text' - The text to calculate the IOC of
/// * 'len' - The length of the text
fn calculate_ioc(text: impl Iterator<Item = u8>, len: usize) -> f64 {
    let top: u32 = freq(text).into_par_iter().map(|v| v.1 * (v.1 - 1)).sum();
    let bot: usize = len * (len - 1);
    f64::from(top) * 26. / bot as f64
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_encrypt_decrypt() {
        let pt: &[u8] = b"THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG";
        let ct: &[u8] = b"DLCAYGMOZBSUXJMHNSWTCNSTOVRRIJKDWNSE";
        let key = Key::new(b"KEY".to_vec());
        assert_eq!(key.encrypt(pt), ct);
        assert_eq!(key.decrypt(ct), pt);
    }

    #[test]
    fn test_guess_key_len() {
        let ct = b"IVRTEZQAVTLHTMJINKATHULDUHPCZFOZLDPKUGKHLLFVVOWVBIIIESZOEDGAOHFRTSZGFCTSXQFDPKEVFUWVZCKBPXAZCOHWPPCIYVXMSUEKTCLLOFAHSELLFOTKPVNZZNODKSZTSWDHYEJKTCLLOTQRLSEWPCWFLFPFVEISYWEEOXMWILJSZRZMASDHZAWDKHFSPWIVRTEZQMNECWFFPIYYFCRCNGYDCIDZMBUWSSFHYEJVURRCEMMZCYLUOCDPWAEVKOHSDRSUTDPWEGLKFFFNRWDGFCTWFMNEXMEHLNOWDGKAYVFVRTHZQBRSZUUSKYFFPSIMTFQGZNEWXZVCEMMZWRPWPCDFZJUHJOHFBIIPZKQGZTTKMPJOWMFSCYXGDOCLJTMRSUEOTSEIEJQDIEDKQGSIZDAUZCLDRFVEOGYTFRTLECNNAMDDFSPKUHZSLTECCUEWXMDOCSXZPGZGPHYEDWYCIAWTMRJAYVSCFDDSDSEOEBGGKCFKFCDSEZQMRRPSEFVAWSEFFCVKMBUTCWQGKHPVQGKRFUFWMEDQYDRTSQNMZNEWXZVCEMMZJTZOMFULLOXSJSYWEGZNEZQGZXEAQGRNOKUBTETKPSIIGWPBFDZMNHWRZEIVRTTKBSICPAHSUTZTQOTOXEABVNPEKHYEDGOWRLDQEHVMMMFHYEXWFOGHJKUQJOQIGOCIEQOCECWMPSJTSSFHYIDKKAGAEZKKRSCWMZCYDLGDZDEZQRVCLVQGJIYUQHYEDAJHZEDZMJVBZJZSKHTKAIK";
        assert_eq!(guess_key_len(ct, 10), 6);
    }
}
