///! Affine Cipher
///
/// A simple monoalphabetic substition cipher.
/// The affine cipher is equivalent to a single
/// modular multiplication and addition.
use crate::analysis::{par_freq, prob_monogram};
use crate::error::{CryptoError, CryptoResult};
use crate::prelude::*;
use crate::utils::{self, pairs_ord};
use rayon::prelude::*;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Key {
    a: M26,
    b: M26,

    a_inv: M26,
}

impl Encrypt for Key {
    type CodePoint = u8;

    fn encrypt(&self, pt: &[u8]) -> Vec<u8> {
        pt.par_iter()
            .cloned()
            .map(|cp| self.encrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

impl Decrypt for Key {
    type CodePoint = u8;

    fn decrypt(&self, ct: &[u8]) -> Vec<u8> {
        ct.par_iter()
            .cloned()
            .map(|cp| self.decrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

impl Key {
    pub fn new(a: u32, b: u32) -> CryptoResult<Self> {
        let a = M26::new(a);
        let b = M26::new(b);
        let a_inv = a.inverse().ok_or(CryptoError::InvalidKey)?;
        Ok(Key { a, b, a_inv })
    }

    pub fn encrypt_cp(&self, cp: u8) -> Option<u8> {
        let i = M26::from_ascii(cp)?;
        let i = self.a * i + self.b;
        Some(i.into_ascii())
    }

    pub fn decrypt_cp(&self, cp: u8) -> Option<u8> {
        let i = M26::from_ascii(cp)?;
        let i = self.a_inv * (i - self.b);
        Some(i.into_ascii())
    }
}

/// Returns the key used to decrypt the affine cipher.
///
/// # Arguments
///
/// * 'text' - The cipher-text to decode
pub fn crack(text: &[u8]) -> Key {
    let freq = par_freq(
        text.par_iter()
            .filter(|c| c.is_ascii_alphabetic())
            .map(u8::to_ascii_uppercase),
    );

    pairs_ord(0, 26)
        .map(|k| Key::new(u32::from(k.0), u32::from(k.1)))
        .filter_map(Result::ok)
        .map(|k| (k, fitness(k, &freq, text.len())))
        .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
        .unwrap()
        .0
}

fn fitness(key: Key, freq: &HashMap<u8, u32>, len: usize) -> f64 {
    utils::fitness::simple(freq.clone(), len, |c| {
        prob_monogram(key.decrypt_cp(c).unwrap())
    })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_encrypt_decrypt() {
        let pt: &[u8] = b"THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG";
        let ct: &[u8] = b"ZRCKEWSGNPAOVHATBEQFCXAJCPZRCLIDYXAM";
        let key = Key::new(5, 8).unwrap();
        assert_eq!(key.encrypt(pt).as_slice(), ct);
        assert_eq!(key.decrypt(ct).as_slice(), pt);
    }
}
