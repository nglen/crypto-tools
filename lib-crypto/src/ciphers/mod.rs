pub mod affine;
pub mod hill;
pub mod lfsr;
pub mod rsa;
pub mod shift;
pub mod substitution;
pub mod vigenere;

pub trait Encrypt {
    type CodePoint;

    /// Encrypts a group of codepoints.
    fn encrypt(&self, pt: &[Self::CodePoint]) -> Vec<Self::CodePoint>;
}

pub trait Decrypt {
    type CodePoint;

    /// Decrypts a group of codepoints.
    fn decrypt(&self, ct: &[Self::CodePoint]) -> Vec<Self::CodePoint>;
}
