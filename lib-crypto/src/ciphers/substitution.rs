///! Substitution Cipher
///
/// The monoalphabetic substitution cipher works
/// by replacing each character of the plaintext
/// with a corresponding output character.
use crate::analysis::{par_freq, prob_bigram, prob_monogram};
use crate::prelude::*;
use crate::utils::{self, mutate, pairs, swap_bytes};
use rayon::prelude::*;
use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Key {
    text: Vec<u8>,
}

impl Key {
    pub fn new(text: Vec<u8>) -> Self {
        Self { text }
    }

    pub fn as_str(&self) -> Result<&str, std::str::Utf8Error> {
        std::str::from_utf8(&self.text)
    }

    fn encrypt_cp(&self, cp: u8) -> Option<u8> {
        let i = M26::from_ascii(cp)?.into_u32();
        Some(self.text[i as usize])
    }

    fn decrypt_cp(&self, cp: u8) -> Option<u8> {
        let i = self.text.iter().cloned().position(|v| v == cp)?;
        Some(M26::new(i as u32).into_ascii())
    }

    fn fitness(&self, freq: &HashMap<(u8, u8), u32>, len: usize) -> f64 {
        utils::fitness::simple(freq.clone(), len, |cp| {
            prob_bigram(self.decrypt_pair(cp).unwrap())
        })
    }

    fn fitness_swap(&self, freq: &HashMap<(u8, u8), u32>, len: usize, to_swap: (u8, u8)) -> f64 {
        utils::fitness::relative(
            freq.clone(),
            len,
            |&cp| prob_bigram(self.decrypt_pair(cp).unwrap()),
            |&cp| prob_bigram(self.decrypt_pair(swap_key_pair(cp, to_swap)).unwrap()),
            |cp| cp.0 == to_swap.0 || cp.0 == to_swap.1 || cp.1 == to_swap.0 || cp.1 == to_swap.1,
        )
    }

    fn mutate(&self) -> Key {
        Key {
            text: mutate(&self.text),
        }
    }

    fn decrypt_pair(&self, text: (u8, u8)) -> Option<(u8, u8)> {
        Some((self.decrypt_cp(text.0)?, self.decrypt_cp(text.1)?))
    }
}

impl Encrypt for Key {
    type CodePoint = u8;

    fn encrypt(&self, pt: &[u8]) -> Vec<u8> {
        pt.par_iter()
            .cloned()
            .map(|cp| self.encrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

impl Decrypt for Key {
    type CodePoint = u8;

    fn decrypt(&self, ct: &[u8]) -> Vec<u8> {
        ct.par_iter()
            .cloned()
            .map(|cp| self.decrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

/// Returns the key used to decrypt the substitution cipher.
///
/// # Arguments
///
/// * 'text' - The cipher-text to decode
pub fn crack(text: &[u8]) -> Key {
    let key = guess_key(text);
    let len = text.len() * 2 - 2;

    let filtered: Vec<_> = text
        .par_iter()
        .filter(|pt| pt.is_ascii_alphabetic())
        .map(u8::to_ascii_uppercase)
        .collect();
    let freq = par_freq(filtered.as_slice().par_windows(2).map(|a| (a[0], a[1])));

    (0..20)
        .fold((std::f64::NEG_INFINITY, key), |acc, _| {
            let new_key = improve_key(&acc.1.mutate(), &freq, len);
            let new_fitness = new_key.fitness(&freq, len);

            if new_fitness > acc.0 {
                (new_fitness, new_key)
            } else {
                acc
            }
        })
        .1
}

fn swap_key_char(c: u8, to_swap: (u8, u8)) -> u8 {
    if c == to_swap.0 {
        to_swap.1
    } else if c == to_swap.1 {
        to_swap.0
    } else {
        c
    }
}

fn swap_key_pair(pair: (u8, u8), to_swap: (u8, u8)) -> (u8, u8) {
    (
        swap_key_char(pair.0, to_swap),
        swap_key_char(pair.1, to_swap),
    )
}

/// Guesses the key in an efficient manner.
fn guess_key(text: &[u8]) -> Key {
    let alphabet = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    let mut freq: Vec<_> = par_freq(
        text.par_iter()
            .filter(|c| c.is_ascii_alphabetic())
            .map(u8::to_ascii_uppercase),
    )
    .into_par_iter()
    .collect();

    freq.par_sort_unstable_by(|a, b| a.1.cmp(&b.1));
    let freq = freq.into_par_iter().map(|a| a.0);

    let prob = {
        let mut prob: Vec<_> = alphabet
            .par_iter()
            .map(|&c| (c, prob_monogram(c)))
            .collect();
        prob.par_sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        prob.into_par_iter().map(|a| a.0)
    };

    let zip: Vec<_> = freq.zip(prob).collect();

    Key {
        text: alphabet
            .par_iter()
            .map(|&c| {
                zip.par_iter()
                    .find_any(|a| a.0 == c)
                    .map(|a| a.1)
                    .unwrap_or(c)
            })
            .collect(),
    }
}

/// Takes a key and constantly improves it's
/// fitness until a local maxima is reached.
fn improve_key(key: &Key, freq: &HashMap<(u8, u8), u32>, len: usize) -> Key {
    let mut key = key.to_owned();
    loop {
        let best = pairs(b'A', b'Z')
            .map(|a| (a, key.fitness_swap(freq, len, a)))
            .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
            .unwrap();

        if best.1 > 0. {
            swap_bytes(&mut key.text, best.0);
        } else {
            break key;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_encrypt_decrypt() {
        let pt: &[u8] = b"THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG";
        let ct: &[u8] = b"HTZOAKFLDPNUGINWQACRZENSZPHTZMBVXENJ";
        let key = Key::new(b"BDFEZIJTKQLMCGNROPYHASUWXV".to_vec());
        assert_eq!(key.encrypt(pt), ct);
        assert_eq!(key.decrypt(ct), pt);
    }
}
