///! RSA Crypto-System
use crate::prelude::*;
use crate::utils::primes::factor;
use crate::utils::{inv_mod, pow_mod};
use rand::{rngs::OsRng, Rng};
use rayon::prelude::*;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct PublicKey {
    /// Modulus
    n: u32,
    /// Public Exponent
    e: u32,
}

impl PublicKey {
    pub fn new(n: u32, e: u32) -> Self {
        Self { n, e }
    }

    /// Returns the corresponding private key for the given public key.
    pub fn crack(self) -> Option<PrivateKey> {
        let (p, q) = factor(self.n)?;
        PrivateKey::new(p, q, self.e)
    }
}

impl Encrypt for PublicKey {
    type CodePoint = u32;

    fn encrypt(&self, pt: &[u32]) -> Vec<u32> {
        pt.par_iter()
            .cloned()
            .map(|cp| pow_mod(cp, self.e, self.n))
            .collect()
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct PrivateKey {
    /// Modulus
    n: u32,
    /// First Prime Factor
    p: u32,
    /// Second Prime Factor
    q: u32,
    /// Private Exponent
    d: u32,
    /// Public Exponent
    e: u32,
}

#[allow(clippy::many_single_char_names)]
impl PrivateKey {
    pub fn new(p: u32, q: u32, e: u32) -> Option<Self> {
        let n = p.checked_mul(q)?;
        let phi_n = n - p - q + 1;

        let d = inv_mod(e, phi_n)?;
        Some(Self { n, p, q, d, e })
    }

    /// Generates a private key from two prime numbers.
    pub fn generate(p: u32, q: u32) -> Option<Self> {
        let n = p.checked_mul(q)?;
        let phi_n = n - p - q + 1;

        let mut rng = OsRng::new().expect("Failed to create OsRng");
        let (d, e) = (0..)
            .filter_map(|_| {
                let d = rng.gen_range(1, phi_n);
                let e = inv_mod(d, phi_n)?;
                Some((d, e))
            })
            .next()?;

        Some(Self { n, p, q, d, e })
    }

    /// Returns the corresponding public key.
    pub fn public(&self) -> PublicKey {
        PublicKey {
            n: self.n,
            e: self.e,
        }
    }
}

impl Encrypt for PrivateKey {
    type CodePoint = u32;

    fn encrypt(&self, pt: &[u32]) -> Vec<u32> {
        pt.par_iter()
            .cloned()
            .map(|cp| pow_mod(cp, self.e, self.n))
            .collect()
    }
}

impl Decrypt for PrivateKey {
    type CodePoint = u32;

    fn decrypt(&self, ct: &[u32]) -> Vec<u32> {
        ct.par_iter()
            .cloned()
            .map(|cp| pow_mod(cp, self.d, self.n))
            .collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_private_key_new() {
        let key = PrivateKey::new(7, 11, 13).unwrap();
        assert_eq!(
            key,
            PrivateKey {
                n: 77,
                p: 7,
                q: 11,
                e: 13,
                d: 37,
            }
        );
    }

    #[test]
    fn test_public_from_private() {
        let priv_key = PrivateKey::new(11, 7, 13).unwrap();
        let pub_key = priv_key.public();
        assert_eq!(pub_key, PublicKey { n: 77, e: 13 });
    }

    #[test]
    fn test_crack_key() {
        let priv_key = PrivateKey::new(11, 7, 13).unwrap();
        let pub_key = priv_key.public();
        assert_eq!(pub_key.crack(), Some(priv_key));
    }

    #[test]
    fn test_encrypt() {
        let pt = &[0, 1, 2, 3, 4, 5, 6, 65];
        let ct = &[0, 1, 30, 38, 53, 26, 62, 65];
        let priv_key = PublicKey::new(77, 13);
        let pub_key = PublicKey::new(77, 13);
        assert_eq!(priv_key.encrypt(pt), ct);
        assert_eq!(pub_key.encrypt(pt), ct);
    }

    #[test]
    fn test_decrypt() {
        let key = PrivateKey::new(11, 7, 13).unwrap();
        let pt: Vec<_> = (0..77).collect();
        let ct = key.encrypt(&pt);
        assert_eq!(key.decrypt(&ct), pt);
    }
}
