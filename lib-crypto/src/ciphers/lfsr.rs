///! LSFR-based Stream Cipher
///
/// A simple stream cipher.
/// This lsfr-based cipher is obtained by mearly
/// xoring the plaintext with the output of a
/// linear feedback shift register.
use crate::prelude::*;

#[derive(Debug, Clone)]
pub struct Key {
    init: Vec<bool>,
    coef: Vec<bool>,
}

impl Key {
    pub fn new(init: Vec<bool>, coef: Vec<bool>) -> Key {
        Key { init, coef }
    }

    pub fn iter(&self) -> KeyIter {
        KeyIter {
            state: self.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct KeyIter {
    state: Key,
}

impl Iterator for KeyIter {
    type Item = bool;

    fn next(&mut self) -> Option<bool> {
        let cur = self.state.init[0];
        self.state.init[0] = self
            .state
            .init
            .iter()
            .zip(self.state.coef.iter())
            .filter(|v| *v.1)
            .map(|v| v.0)
            .fold(false, |acc, x| acc ^ x);
        self.state.init.rotate_left(1);
        Some(cur)
    }
}

impl Encrypt for Key {
    type CodePoint = bool;

    fn encrypt(&self, pt: &[bool]) -> Vec<bool> {
        let tmp2 = self.clone();

        pt.iter().zip(tmp2.iter()).map(|(v, k)| v ^ k).collect()
    }
}

impl Decrypt for Key {
    type CodePoint = bool;

    fn decrypt(&self, ct: &[bool]) -> Vec<bool> {
        self.encrypt(ct)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_encrypt_decrypt() {
        // TODO: Implement a test for this
    }

    #[test]
    pub fn key_iter() {
        let key = Key {
            init: vec![true, false, false, false, false],
            coef: vec![true, false, true, false, false],
        };

        let expected: &[bool] = &[
            true, false, false, false, false, true, false, false, true, false, true, true, false,
            false, true, true, true, true, true, false,
        ];

        let out: Vec<bool> = key.iter().take(expected.len()).collect();

        assert_eq!(out, expected);
    }
}
