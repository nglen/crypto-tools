///! Hill Cipher
///
/// A polyalphabetic substitution cipher.
/// The key is equivalent to a matrix and
/// encryption is equivalent to modular
/// multiplication by that matrix.
///
/// For simplicities sake, this implementation
/// only supports 2x2 matricies.
use crate::analysis::{par_freq, prob_bigram};
use crate::prelude::*;
use crate::utils::fitness::simple as fit_simple;
use rayon::prelude::*;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Key {
    tl: M26,
    tr: M26,
    bl: M26,
    br: M26,
}

impl Key {
    pub fn new(tl: u32, tr: u32, bl: u32, br: u32) -> Option<Self> {
        let tl = M26::new(tl);
        let tr = M26::new(tr);
        let bl = M26::new(bl);
        let br = M26::new(br);

        let key = Self { tl, tr, bl, br };
        key.inverse()?; // Ensure the key is invertable

        Some(key)
    }

    pub fn determinant(&self) -> Option<M26> {
        let v = self.tl * self.br - self.tr * self.bl;
        v.inverse()
    }

    pub fn inverse(&self) -> Option<Self> {
        let det = self.determinant()?;

        Some(Self {
            tl: det * self.br,
            tr: det * -self.tr,
            bl: det * -self.bl,
            br: det * self.tl,
        })
    }

    pub fn multiply_vec(&self, lhs: &[M26]) -> [M26; 2] {
        let x = self.tl * lhs[0] + self.tr * lhs[1];
        let y = self.bl * lhs[0] + self.br * lhs[1];
        [x, y]
    }
}

fn encrypt_pair(text: (M26, M26), key: &Key) -> [u8; 2] {
    let ret = key.multiply_vec(&[text.0, text.1]);
    [ret[0].into_ascii(), ret[1].into_ascii()]
}

impl Encrypt for Key {
    type CodePoint = u8;

    fn encrypt(&self, pt: &[u8]) -> Vec<u8> {
        let pt: Vec<_> = pt.par_iter().cloned().filter_map(M26::from_ascii).collect();
        pt.chunks_exact(2)
            .flat_map(|cp| self.multiply_vec(cp).to_vec())
            .map(M26::into_ascii)
            .collect()
    }
}

impl Decrypt for Key {
    type CodePoint = u8;

    fn decrypt(&self, ct: &[u8]) -> Vec<u8> {
        self.inverse().unwrap().encrypt(ct)
    }
}

/// Returns the key used to decrypt the hill cipher.
///
/// # Arguments
///
/// * 'text' - The cipher-text to decode
pub fn crack(text: &[u8]) -> Key {
    let len = text.len() * 2;
    let text: Vec<_> = text
        .par_iter()
        .filter_map(|&c| M26::from_ascii(c))
        .collect();
    let text: Vec<_> = text.chunks_exact(2).collect();

    let freq = par_freq(text.into_par_iter().map(|a| (a[0], a[1])));

    // This is to many nested loops not to use a macro.
    // Luckilly, rust macros are fully syntaxed instead
    // of simple text replacement like c macros.
    macro_rules! max {
        ($f:expr) => {
            (0..26u32)
                .into_par_iter()
                .filter_map($f)
                .max_by(|x, y| x.1.partial_cmp(&y.1).unwrap())
        };
    }
    max!(|a| max!(|b| max!(|c| max!(|d| {
        let key = Key::new(a, b, c, d)?;
        let fitness = fitness(&key, &freq, len)?;
        Some((key, fitness))
    }))))
    .unwrap()
    .0
}

#[allow(clippy::implicit_hasher)]
pub fn fitness(key: &Key, freq: &HashMap<(M26, M26), u32>, len: usize) -> Option<f64> {
    let inv_key = key.inverse()?;
    Some(fit_simple(freq.clone(), len, |c| {
        let ct = encrypt_pair(c, &inv_key);
        prob_bigram((ct[0], ct[1]))
    }))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_encrypt_decrypt() {
        let pt: &[u8] = b"THEQUICKBROWNFOXJUMPEDOVERTHELAZYDOG";
        let ct: &[u8] = b"AVIKGCKCCJEICZHNJODVVXBDLPAVTLXVDLIG";
        let key = Key::new(3, 3, 2, 5).unwrap();
        assert_eq!(key.encrypt(pt).as_slice(), ct);
        assert_eq!(key.decrypt(ct).as_slice(), pt);
    }
}
