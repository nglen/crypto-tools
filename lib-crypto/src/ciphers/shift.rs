use crate::analysis::{par_freq, prob_monogram};
///! Shift Cipher
///
/// The shift cipher operates by treating each
/// character as a number and encryption as
/// modular addition.
use crate::prelude::*;
use crate::utils;
use rayon::prelude::*;
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Key {
    pub(crate) shift: M26,
}

impl Key {
    pub fn new(shift: u32) -> Self {
        Self {
            shift: M26::new(shift),
        }
    }

    #[inline]
    fn encrypt_cp(self, cp: u8) -> Option<u8> {
        let i = M26::from_ascii(cp)?;
        let i = i + self.shift;
        Some(i.into_ascii())
    }

    #[inline]
    fn decrypt_cp(self, cp: u8) -> Option<u8> {
        let i = M26::from_ascii(cp)?;
        let i = i - self.shift;
        Some(i.into_ascii())
    }

    fn fitness(self, freq: &HashMap<u8, u32>, len: usize) -> f64 {
        utils::fitness::simple(freq.clone(), len, |c| {
            prob_monogram(self.decrypt_cp(c).unwrap())
        })
    }
}

impl Encrypt for Key {
    type CodePoint = u8;

    fn encrypt(&self, pt: &[u8]) -> Vec<u8> {
        pt.par_iter()
            .cloned()
            .map(|cp| self.encrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

impl Decrypt for Key {
    type CodePoint = u8;

    fn decrypt(&self, ct: &[u8]) -> Vec<u8> {
        ct.par_iter()
            .cloned()
            .map(|cp| self.decrypt_cp(cp).unwrap_or(cp))
            .collect()
    }
}

/// Returns the key used to decrypt the shift cipher.
///
/// # Arguments
///
/// * 'text' - The cipher-text to decode
pub fn crack(text: &[u8]) -> Key {
    let freq = par_freq(
        text.par_iter()
            .filter(|c| c.is_ascii_alphabetic())
            .map(u8::to_ascii_uppercase),
    );

    // Iterate over all possible keys
    (0..26u32)
        .into_par_iter()
        .map(Key::new)
        .map(|k| (k, k.fitness(&freq, text.len())))
        .max_by(|a, b| a.1.partial_cmp(&b.1).unwrap())
        .map(|v| v.0)
        .unwrap()
}
