use rayon::prelude::*;

/// Calculates a simple fitness value
///
/// # Arguments
///
/// * 'freq' - An iterator over each element and its frequency
/// * 'len' - The total frequency of all elements
/// * 'prob' - A function which takes in an element and returns the logarithm of the probability of it occuring
///
/// *Note*: A higher fitness corresponds to a better fit
pub fn simple<E, F>(freq: impl IntoParallelIterator<Item = (E, u32)>, len: usize, prob: F) -> f64
where
    E: Send + Sync,
    F: Fn(E) -> f64 + Send + Sync,
{
    freq.into_par_iter()
        .map(|(c, n)| f64::from(n) / (len as f64) * prob(c))
        .sum()
}

/// Calculates a relative fitness value
///
/// # Arguments
///
/// * 'freq' - An iterator over each element and its frequency
/// * 'len' - The total frequency of all elements
/// * 'prob' - A function which takes in an element and returns the logarithm of the probability of it occuring
/// * 'dirty' - A function which takes in an element and returns whether it has changed
///
/// *Note*: A higher fitness corresponds to a better fit
pub fn relative<E>(
    freq: impl IntoParallelIterator<Item = (E, u32)>,
    len: usize,
    prob_old: impl Fn(&E) -> f64 + Send + Sync,
    prob_new: impl Fn(&E) -> f64 + Send + Sync,
    dirty: impl Fn(&E) -> bool + Send + Sync,
) -> f64
where
    E: Send + Sync,
{
    simple(freq.into_par_iter().filter(|(c, _)| dirty(c)), len, |c| {
        prob_new(&c) - prob_old(&c)
    })
}
