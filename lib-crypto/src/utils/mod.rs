pub mod fitness;
pub mod modular;
pub mod primes;
pub mod generic_mod;
pub mod integers;

use rand::{thread_rng, Rng};
use rayon::prelude::*;

/// Swap two bytes within a slice.
///
/// # Examples
///
/// ```
/// use lib_crypto::utils::swap_bytes;
///
/// let mut data = b"Hello".to_owned();
/// swap_bytes(&mut data, (b'e', b'o'));
///
/// assert_eq!(&data, b"Holle");
/// ```
pub fn swap_bytes(data: &mut [u8], to_swap: (u8, u8)) {
    if let Some(i) = data.iter().position(|&v| v == to_swap.0) {
        if let Some(j) = data.iter().position(|&v| v == to_swap.1) {
            data.swap(i, j);
        }
    }
}

/// Returns a parallel iterator over all ordered pairs between two values
///
/// Operates over the exclusive range (min..max)
pub fn pairs_ord(min: u8, max: u8) -> impl ParallelIterator<Item = (u8, u8)> {
    (min..max)
        .into_par_iter()
        .flat_map(move |i| (min..max).into_par_iter().map(move |j| (i, j)))
}

/// Returns a parallel iterator over all unordered pairs between two values
///
/// Operates over the inclusive range (min..=max) where a.0 != a.1
#[allow(clippy::range_plus_one)]
pub fn pairs(min: u8, max: u8) -> impl ParallelIterator<Item = (u8, u8)> {
    (min..max)
        .into_par_iter()
        .flat_map(move |i| (i + 1..max + 1).into_par_iter().map(move |j| (i, j)))
}

/// Returns a moddified Vec such that three random
/// bytes located within the data slice are swapped.
pub fn mutate(data: &[u8]) -> Vec<u8> {
    match data.len() {
        0 => data.to_vec(),
        1 => data.to_vec(),
        2 => vec![data[1], data[0]],
        _ => {
            let mut ret = data.to_vec();
            let mut rng = thread_rng();

            let i = rng.gen_range(0, data.len() - 1);
            let j = rng.gen_range(0, data.len() - 1);
            let k = rng.gen_range(0, data.len() - 1);

            ret.as_mut_slice().swap(i, j);
            ret.as_mut_slice().swap(i, k);
            ret
        }
    }
}

/// Returns the results of the Extended Euclidean Algorithm
///
/// The result is stored as a touple of the form (gcd, x, y)
/// where a*x + b*y = gcd
pub fn gcd_extended(a: u32, b: u32) -> (u32, i32, i32) {
    if a == 0 {
        (b, 0, 1)
    } else {
        // FIXME: Don't use recursion as the stack may overflow
        let (gcd, x, y) = gcd_extended(b % a, a);
        (gcd, y - (b / a) as i32 * x, x)
    }
}

/// Returns the greatest common denominator of two numbers.
pub fn gcd(mut a: u32, mut b: u32) -> u32 {
    while b != 0 {
        let tmp = a;
        a = b;
        b = tmp % b;
    }
    a
}

/// Returns whether the two numbers given are coprime.
pub fn coprime(a: u32, b: u32) -> bool {
    gcd(a, b) == 1
}

/// Returns the modular multiplicative inverse of it exists or none if it doesn't.
pub fn inv_mod(val: u32, m: u32) -> Option<u32> {
    if !coprime(val, m) {
        None
    } else if m == 1 {
        Some(1)
    } else {
        let (_gcd, x, _y) = gcd_extended(val, m);

        if x < 0 {
            Some((x + m as i32) as u32)
        } else {
            Some(x as u32)
        }
    }
}

/// Returns the modular power of two numbers.
pub fn pow_mod(val: u32, exp: u32, m: u32) -> u32 {
    let val = u64::from(val);
    let exp = u64::from(exp);
    let m = u64::from(m);

    if val == 0 {
        0
    } else {
        let mut z = 1;
        let mut cur_bit = 1 << (binary_len(exp as u32) - 1);
        while cur_bit != 0 {
            z = (z * z) % m;

            if (cur_bit & exp) != 0 {
                z = (z * val) % m;
            }

            cur_bit >>= 1;
        }
        z as u32
    }
}

/// Returns the modular subtraction of two numbers.
pub fn sub_mod(lhs: u32, rhs: u32, m: u32) -> u32 {
    if lhs > rhs {
        (lhs - rhs) % m
    } else {
        (rhs - lhs) % m
    }
}

/// Returns the number of binary digits necessary to represent a number.
///
/// If the number is zero, this will return 1.
pub fn binary_len(val: u32) -> u32 {
    if val == 0 {
        1
    } else {
        32 - val.leading_zeros()
    }
}

pub trait FloatIterExt {
    fn float_min(&mut self) -> f64;
    fn float_max(&mut self) -> f64;
}
impl<T> FloatIterExt for T
where
    T: Iterator<Item = f64>,
{
    fn float_max(&mut self) -> f64 {
        self.fold(std::f64::NAN, f64::max)
    }

    fn float_min(&mut self) -> f64 {
        self.fold(std::f64::NAN, f64::min)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_gcd() {
        let simple_test = |a, b, ret| {
            assert_eq!(gcd(a, b), ret);
        };

        simple_test(0, 0, 0);
        simple_test(100, 0, 100);
        simple_test(100, 1, 1);
        simple_test(100, 2, 2);
        simple_test(100, 3, 1);
        simple_test(100, 40, 20);
        simple_test(100, 51, 1);
        simple_test(0, 100, 100);
        simple_test(1, 100, 1);
        simple_test(2, 100, 2);
        simple_test(3, 100, 1);
        simple_test(40, 100, 20);
        simple_test(51, 100, 1);
    }

    #[test]
    fn test_gcd_extended() {
        let simple_test = |a, b, gcd| {
            let tmp = gcd_extended(a, b);
            assert_eq!(tmp.0, gcd);
            assert_eq!(tmp.0 as i32, tmp.1 * a as i32 + tmp.2 * b as i32);
        };

        simple_test(0, 0, 0);
        simple_test(100, 0, 100);
        simple_test(100, 1, 1);
        simple_test(100, 2, 2);
        simple_test(100, 3, 1);
        simple_test(100, 40, 20);
        simple_test(100, 51, 1);
        simple_test(0, 100, 100);
        simple_test(1, 100, 1);
        simple_test(2, 100, 2);
        simple_test(3, 100, 1);
        simple_test(40, 100, 20);
        simple_test(51, 100, 1);
    }

    #[test]
    fn test_coprime() {
        let simple_test = |a, b, res| {
            assert_eq!(coprime(a, b), res);
        };

        simple_test(0, 0, false);
        simple_test(100, 0, false);
        simple_test(100, 1, true);
        simple_test(100, 2, false);
        simple_test(100, 3, true);
        simple_test(100, 40, false);
        simple_test(100, 51, true);
        simple_test(0, 100, false);
        simple_test(1, 100, true);
        simple_test(2, 100, false);
        simple_test(3, 100, true);
        simple_test(40, 100, false);
        simple_test(51, 100, true);
    }

    #[test]
    fn test_pow_mod() {
        assert_eq!(pow_mod(63, 3, 63_u32.pow(3) + 1), 63_u32.pow(3));
        assert_eq!(pow_mod(63, 3, 63_u32.pow(3)), 0_u32);
        assert_eq!(pow_mod(63, 3, 63_u32.pow(3) - 1), 1_u32);
        assert_eq!(pow_mod(2, 13, 77), 30);
    }

    #[test]
    fn test_binary_len() {
        assert_eq!(binary_len(0), 1);
        assert_eq!(binary_len(1), 1);
        assert_eq!(binary_len(2), 2);
        assert_eq!(binary_len(3), 2);
        assert_eq!(binary_len(4), 3);
        assert_eq!(binary_len(5), 3);
    }

    #[test]
    fn test_mutate_small() {
        assert_eq!(mutate(&[]), &[]);
        assert_eq!(mutate(&[1]), &[1]);
        mutate(&[1, 2]);
    }

    #[test]
    fn test_swap_bytes_small() {
        swap_bytes(&mut [], (0, 1));
        swap_bytes(&mut [0], (0, 1));
        swap_bytes(&mut [2], (0, 1));
        swap_bytes(&mut [0, 1], (0, 1));
        swap_bytes(&mut [0, 2], (0, 1));
    }
}
