use crate::utils::inv_mod;
use std::fmt;
use std::ops::{Add, AddAssign, Mul, MulAssign, Neg, Sub, SubAssign};

// We use a u32 because 25*25 > 255 so
// multiplication over a u8 might overflow
/// A Structure designed for use in modular arithmatic.
///
/// A M26 is closed under modular 26 operations such
/// as addition, subtraction, and multiplication.
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct M26(u32);

impl M26 {
    pub fn new(val: u32) -> Self {
        Self(val % 26)
    }

    /// Converts a u8 into it's corresponding m26 value.
    /// If the u8 is not a valid alphabetic ascii character,
    /// it will return None.
    pub fn from_ascii(val: u8) -> Option<Self> {
        if !val.is_ascii_alphabetic() {
            None
        } else {
            Some(Self::new(u32::from(val.to_ascii_uppercase() - b'A')))
        }
    }

    pub fn into_u32(self) -> u32 {
        self.0
    }

    /// Converts a m26 into it's corresponding uppercase ascii character.
    pub fn into_ascii(self) -> u8 {
        (self.0 as u8) + b'A'
    }

    pub fn inverse(self) -> Option<M26> {
        Some(Self::new(inv_mod(self.0, 26)?))
    }
}

impl fmt::Display for M26 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Debug for M26 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Add for M26 {
    type Output = M26;

    fn add(self, rhs: M26) -> M26 {
        M26::new(self.0 + rhs.0)
    }
}

impl Sub for M26 {
    type Output = M26;

    fn sub(self, rhs: M26) -> M26 {
        if self.0 > rhs.0 {
            M26::new(self.0 - rhs.0)
        } else {
            M26::new((26 + self.0) - rhs.0)
        }
    }
}

impl Mul for M26 {
    type Output = M26;

    fn mul(self, rhs: M26) -> M26 {
        M26::new(self.0 * rhs.0)
    }
}

impl Neg for M26 {
    type Output = M26;

    fn neg(self) -> M26 {
        M26::new(0) - self
    }
}

impl AddAssign for M26 {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
        self.0 %= 26;
    }
}

impl SubAssign for M26 {
    fn sub_assign(&mut self, rhs: Self) {
        if self.0 > rhs.0 {
            self.0 -= rhs.0;
        } else {
            self.0 += rhs.0;
        }
        self.0 %= 26;
    }
}

impl MulAssign for M26 {
    fn mul_assign(&mut self, rhs: Self) {
        self.0 *= rhs.0;
        self.0 %= 26;
    }
}
