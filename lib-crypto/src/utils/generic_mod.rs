use std::ops::{Add, Sub, Mul, Neg};
use std::fmt;
use crate::utils::{inv_mod, pow_mod};

pub trait Modular {
    fn to_modulo(self, modulus: u32) -> Modulo;
}

impl Modular for u32 {
    fn to_modulo(self, modulus: u32) -> Modulo {
        Modulo::new(self, modulus)
    }
}

impl Modular for Modulo {
    fn to_modulo(self, modulus: u32) -> Modulo {
        Modulo::new(self.value(), modulus)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Modulo {
    value: i64,
    modulus: i64,
}

impl fmt::Display for Modulo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} mod {}", self.value(), self.modulus())
    }
}

impl Modulo {
    pub fn new(value: u32, modulus: u32) -> Self {
        Self {
            value: (value % modulus).into(),
            modulus: modulus.into(),
        }
    }

    pub fn zero(modulus: u32) -> Self {
        Self { value: 0, modulus: modulus.into() }
    }

    pub fn one(modulus: u32) -> Self {
        Self { value: 1, modulus: modulus.into() }
    }

    pub fn value(&self) -> u32 {
        if self.value >= 0 {
            self.value as u32
        } else {
            (self.value + self.modulus) as u32
        }
    }

    pub fn modulus(&self) -> u32 {
        self.modulus as u32
    }
    
    pub fn pow(&self, rhs: impl Into<u32>) -> Self {
        Self {
            value: i64::from(pow_mod(self.value(), rhs.into(), self.modulus())),
            modulus: self.modulus,
        }
    }

    pub fn inv(&self) -> Option<Self> {
        Some(Self {
            value: i64::from(inv_mod(self.value(), self.modulus())?),
            modulus: self.modulus,
        })
    }
}

impl From<Modulo> for u32 {
    fn from(modulo: Modulo) -> u32 {
        modulo.value()
    }
}

impl Add for Modulo {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        assert_eq!(self.modulus, rhs.modulus, "Addition requires modulo numbers with the same modulus");
        Self {
            value: (self.value + rhs.value) % self.modulus,
            modulus: self.modulus,
        }
    }
}

impl Sub for Modulo {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        assert_eq!(self.modulus, rhs.modulus, "Subtraction requires modulo numbers with the same modulus");

        Self {
            value: (self.value - rhs.value) % self.modulus,
            modulus: self.modulus,
        }
    }
}

impl Mul for Modulo {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        assert_eq!(self.modulus, rhs.modulus, "Multiplication requires modulo numbers with the same modulus");
        Self {
            value: (self.value * rhs.value) % self.modulus,
            modulus: self.modulus,
        }
    }
}

impl Neg for Modulo {
    type Output = Self;

    fn neg(self) -> Self {
        Self {
            value: -self.value,
            modulus: self.modulus,
        }
    }
}

impl PartialEq for Modulo {
    fn eq(&self, other: &Modulo) -> bool {
        self.value() == other.value()
    }
}
impl Eq for Modulo {}
