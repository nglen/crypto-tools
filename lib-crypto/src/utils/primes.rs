use crate::utils::integers::IntegerSquareRoot;

/// Calculate two corresponding factors of a given number.
///
/// Returns None when the given number is prime.
/// The left hand side will always be greater
/// than or equal to the right hand side.
pub fn factor(val: u32) -> Option<(u32, u32)> {
    if val % 2 == 0 {
        return Some((val/2, 2));
    }

    let max = val / 4;
    let (d, z) = (0..=max)
        .filter_map(|d| {
            let z = (val + d*d).perfect_sqrt()?;
            Some((d, z))
        }).next()?;

    let p = z+d;
    let q = z-d;
    Some((p, q))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_factor() {
        assert_eq!(factor(2*2), Some((2, 2)));
        assert_eq!(factor(79193*2), Some((79193, 2)));
        assert_eq!(factor(79193*3), Some((79193, 3)));
        assert_eq!(factor(11*11), Some((11, 11)));
    }
}
