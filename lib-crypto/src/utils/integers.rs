pub trait IntegerSquareRoot where Self: Sized {
    fn integer_sqrt(&self) -> Self;
    fn perfect_sqrt(&self) -> Option<Self>;
}

impl IntegerSquareRoot for u32 {
    fn integer_sqrt(&self) -> Self {
        if *self == 0 {
            return 0;
        }

        let mut prev = 1;
        let mut first = true;
        loop {
            let cur = (prev + self / prev) / 2;

            if !first && ((cur == prev) || (cur == prev + 1)) {
                return prev;
            }

            prev = cur;
            first = false;
        }
    }

    fn perfect_sqrt(&self) -> Option<Self> {
        let sqrt = self.integer_sqrt();

        if sqrt*sqrt == *self {
            Some(sqrt)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test_integer_sqrt() {
        assert_eq!(0u32.integer_sqrt(), 0);
        assert_eq!(1u32.integer_sqrt(), 1);
        assert_eq!(2u32.integer_sqrt(), 1);
        assert_eq!(3u32.integer_sqrt(), 1);
        assert_eq!(4u32.integer_sqrt(), 2);
        assert_eq!(5u32.integer_sqrt(), 2);
        assert_eq!(6u32.integer_sqrt(), 2);
        assert_eq!(7u32.integer_sqrt(), 2);
        assert_eq!(8u32.integer_sqrt(), 2);
        assert_eq!(9u32.integer_sqrt(), 3);
        assert_eq!(10u32.integer_sqrt(), 3);
        assert_eq!(11u32.integer_sqrt(), 3);
        assert_eq!(12u32.integer_sqrt(), 3);
        assert_eq!(13u32.integer_sqrt(), 3);
        assert_eq!(14u32.integer_sqrt(), 3);
        assert_eq!(15u32.integer_sqrt(), 3);
        assert_eq!(16u32.integer_sqrt(), 4);
    }

    #[test]
    fn test_perfect_sqrt() {
        assert_eq!(0u32.perfect_sqrt(), Some(0));
        assert_eq!(1u32.perfect_sqrt(), Some(1));
        assert_eq!(2u32.perfect_sqrt(), None);
        assert_eq!(3u32.perfect_sqrt(), None);
        assert_eq!(4u32.perfect_sqrt(), Some(2));
        assert_eq!(5u32.perfect_sqrt(), None);
        assert_eq!(6u32.perfect_sqrt(), None);
        assert_eq!(7u32.perfect_sqrt(), None);
        assert_eq!(8u32.perfect_sqrt(), None);
        assert_eq!(9u32.perfect_sqrt(), Some(3));
        assert_eq!(10u32.perfect_sqrt(), None);
        assert_eq!(11u32.perfect_sqrt(), None);
        assert_eq!(12u32.perfect_sqrt(), None);
        assert_eq!(13u32.perfect_sqrt(), None);
        assert_eq!(14u32.perfect_sqrt(), None);
        assert_eq!(15u32.perfect_sqrt(), None);
        assert_eq!(16u32.perfect_sqrt(), Some(4));
    }
}
