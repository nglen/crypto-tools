//! # Lib Crypto
//!
//! 'lib_crypto' is a collection of utilities
//! designed to aid in the encryption, decryption,
//! and cracking of multiple ciphers.
#![warn(clippy::all)]

pub mod analysis;
pub mod ciphers;
pub mod error;
pub mod prelude;
pub mod signatures;
pub mod utils;
