use failure::Fail;

#[derive(Debug, Fail)]
pub enum CryptoError {
    #[fail(display = "invalid key")]
    InvalidKey,
}

pub type CryptoResult<R> = Result<R, CryptoError>;
