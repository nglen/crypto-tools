use failure::Error;
use lib_crypto::ciphers::shift;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    key: u32,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
    #[structopt(name = "crack")]
    Crack,
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let key = shift::Key::new(k.key);
                let ct = key.encrypt(input);
                write!(output, "{}", std::str::from_utf8(&ct)?)?;
                Ok(())
            }
            Cipher::Decrypt(k) => {
                let key = shift::Key::new(k.key);
                let pt = key.decrypt(input);
                write!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
            Cipher::Crack => {
                let key = shift::crack(input);
                let pt = key.decrypt(input);
                writeln!(output, "{:?}", key)?;
                writeln!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
        }
    }
}
