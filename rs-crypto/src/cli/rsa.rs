use failure::Error;
use lib_crypto::ciphers::rsa;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherPublicKey {
    n: u32,
    e: u32,
}

#[derive(Debug, StructOpt)]
pub struct CipherPrivateKey {
    p: u32,
    q: u32,
    e: u32,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherPublicKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherPrivateKey),
    #[structopt(name = "crack")]
    Crack(CipherPublicKey),
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        let string = std::str::from_utf8(input)?;
        let data: Vec<_> = string
            .split(' ')
            .filter_map(|s| s.parse::<u32>().ok())
            .collect();

        match self {
            Cipher::Encrypt(k) => {
                let key = rsa::PublicKey::new(k.n, k.e);
                let ct = key.encrypt(&data);
                writeln!(output, "{:?}", &ct)?;
                Ok(())
            }
            Cipher::Decrypt(k) => {
                let key = rsa::PrivateKey::new(k.p, k.q, k.e).unwrap();
                let pt = key.decrypt(&data);
                writeln!(output, "{:?}", &pt)?;
                Ok(())
            }
            Cipher::Crack(k) => {
                let key = rsa::PublicKey::new(k.n, k.e).crack().unwrap();
                let pt = u32_to_text(&key.decrypt(&data));
                writeln!(output, "{:?}", &key)?;
                writeln!(output, "{:?}", &pt)?;
                Ok(())
            }
        }
    }
}

fn u32_to_text(text: &[u32]) -> String {
    text.iter()
        .map(|v| {
            let mut tmp = *v;
            let v0 = M26::new(tmp).into_ascii();
            tmp /= 26;
            let v1 = M26::new(tmp).into_ascii();
            tmp /= 26;
            let v2 = M26::new(tmp).into_ascii();
            tmp /= 26;
            let v3 = M26::new(tmp).into_ascii();
            tmp /= 26;
            let v4 = M26::new(tmp).into_ascii();
            tmp /= 26;
            let v5 = M26::new(tmp).into_ascii();

            let bytes = &[v5, v4, v3, v2, v1, v0];
            String::from(std::str::from_utf8(bytes).unwrap())
        })
        .collect()
}
