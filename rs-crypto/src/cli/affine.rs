use failure::Error;
use lib_crypto::ciphers::affine;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    a: u32,
    b: u32,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
    #[structopt(name = "crack")]
    Crack,
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let ct = affine::Key::new(k.a, k.b)?.encrypt(input);
                write!(output, "{}", std::str::from_utf8(&ct)?)?;
                Ok(())
            }
            Cipher::Decrypt(k) => {
                let pt = affine::Key::new(k.a, k.b)?.decrypt(input);
                write!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
            Cipher::Crack => {
                let key = affine::crack(input);
                let pt = key.decrypt(input);
                writeln!(output, "{:?}", key)?;
                writeln!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
        }
    }
}
