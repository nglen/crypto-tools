use failure::{format_err, Error};
use lib_crypto::ciphers::lfsr;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    init: String,
    coef: String,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let pt = bit_string_to_bool_vec(input)?;
                let init = bit_string_to_bool_vec(k.init.as_bytes())?;
                let coef = bit_string_to_bool_vec(k.coef.as_bytes())?;

                let key = lfsr::Key::new(init, coef);
                let ct = key.encrypt(&pt);

                let ct = bool_vec_to_bit_string(&ct);
                writeln!(output, "{}", ct)?;
            }
            Cipher::Decrypt(k) => {
                let ct = bit_string_to_bool_vec(input)?;
                let init = bit_string_to_bool_vec(k.init.as_bytes())?;
                let coef = bit_string_to_bool_vec(k.coef.as_bytes())?;

                let key = lfsr::Key::new(init, coef);
                let pt = key.decrypt(&ct);

                let pt = bool_vec_to_bit_string(&pt);
                writeln!(output, "{}", pt)?;
            }
        }
        Ok(())
    }
}

fn bit_string_to_bool_vec(bit_string: &[u8]) -> Result<Vec<bool>, Error> {
    bit_string
        .iter()
        .filter(|v| !v.is_ascii_control())
        .map(|v| match v {
            b'0' => Ok(false),
            b'1' => Ok(true),
            _ => Err(format_err!("Invalid byte in bit string: {}", v)),
        })
        .collect()
}

fn bool_vec_to_bit_string(bool_vec: &[bool]) -> String {
    bool_vec
        .iter()
        .map(|&v| if v { '1' } else { '0' })
        .collect()
}
