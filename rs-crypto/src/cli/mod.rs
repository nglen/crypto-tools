mod affine;
mod hill;
mod lfsr;
mod rsa;
mod shift;
mod substitution;
mod vigenere;

use failure::Error;
use std::io::Write;
use std::path::PathBuf;
use structopt::StructOpt;

/// A cryptographic system.
///
/// Each one has a corresponding ecoding
/// and decoding process.
#[derive(Debug, StructOpt)]
pub enum CryptoSystem {
    /// Shift Cipher
    #[structopt(name = "shift")]
    Shift(shift::Cipher),
    /// Affine Cipher
    #[structopt(name = "affine")]
    Affine(affine::Cipher),
    /// LFSR based Cipher
    #[structopt(name = "lfsr")]
    LFSR(lfsr::Cipher),
    /// Substitution Cipher
    #[structopt(name = "subst")]
    Substitution(substitution::Cipher),
    /// Hill Cipher
    #[structopt(name = "hill")]
    Hill(hill::Cipher),
    /// Vigenere Cipher
    #[structopt(name = "vigenere")]
    Vigenere(vigenere::Cipher),
    /// RSA Crypto System
    #[structopt(name = "rsa")]
    RSA(rsa::Cipher),
}

impl CryptoSystem {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            CryptoSystem::Shift(v) => v.run(input, output),
            CryptoSystem::Affine(v) => v.run(input, output),
            CryptoSystem::LFSR(v) => v.run(input, output),
            CryptoSystem::Substitution(v) => v.run(input, output),
            CryptoSystem::Hill(v) => v.run(input, output),
            CryptoSystem::Vigenere(v) => v.run(input, output),
            CryptoSystem::RSA(v) => v.run(input, output),
        }
    }
}

/// The command line arguments to pass into the program.
#[derive(Debug, StructOpt)]
#[structopt(
    name = "rs_crypto",
    about = "CLI for handling cryptosystems and analysis"
)]
pub struct Opt {
    /// Crypto System to use
    #[structopt(subcommand)]
    pub system: CryptoSystem,

    /// Input file, defaults to stdin
    #[structopt(long = "input", short = "i", parse(from_os_str))]
    pub input: Option<PathBuf>,

    /// Output file, defaults to stdout
    #[structopt(long = "output", short = "o", parse(from_os_str))]
    pub output: Option<PathBuf>,
}
