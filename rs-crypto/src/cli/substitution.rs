use failure::Error;
use lib_crypto::ciphers::substitution;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    /// The mapping for each character in the alphabet
    ///
    /// 'A' will map to the first character.
    /// 'B' will map to the second character.
    /// 'Z' will map to the twenty-sixth character.
    key: String,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
    #[structopt(name = "crack")]
    Crack,
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let key = substitution::Key::new(k.key.as_bytes().to_vec());
                let ct = key.encrypt(input);
                write!(output, "{}", std::str::from_utf8(&ct)?)?;
                Ok(())
            }
            Cipher::Decrypt(k) => {
                let key = substitution::Key::new(k.key.as_bytes().to_vec());
                let pt = key.decrypt(input);
                write!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
            Cipher::Crack => {
                let key = substitution::crack(input);
                let pt = key.decrypt(input);
                writeln!(output, "{}", key.as_str()?)?;
                writeln!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
        }
    }
}
