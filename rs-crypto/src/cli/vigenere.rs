use failure::Error;
use lib_crypto::ciphers::vigenere;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    key: String,
}

#[derive(Debug, StructOpt)]
pub struct CrackParams {
    max_key_len: usize,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
    #[structopt(name = "crack")]
    Crack(CrackParams),
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let key = vigenere::Key::new(k.key.as_bytes().to_vec());
                let ct = key.encrypt(input);
                write!(output, "{}", std::str::from_utf8(&ct)?)?;
                Ok(())
            }
            Cipher::Decrypt(k) => {
                let key = vigenere::Key::new(k.key.as_bytes().to_vec());
                let pt = key.decrypt(input);
                write!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
            Cipher::Crack(params) => {
                let key = vigenere::crack(input, params.max_key_len + 1);
                let pt = key.decrypt(input);
                writeln!(output, "{}", key.as_str()?)?;
                writeln!(output, "{}", std::str::from_utf8(&pt)?)?;
                Ok(())
            }
        }
    }
}
