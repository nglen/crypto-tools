use failure::Error;
use lib_crypto::ciphers::hill;
use lib_crypto::prelude::*;
use std::io::Write;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct CipherKey {
    a: u32,
    b: u32,
    c: u32,
    d: u32,
}

#[derive(Debug, StructOpt)]
pub enum Cipher {
    #[structopt(name = "encrypt")]
    Encrypt(CipherKey),
    #[structopt(name = "decrypt")]
    Decrypt(CipherKey),
    #[structopt(name = "crack")]
    Crack,
}

impl Cipher {
    pub fn run(&self, input: &[u8], output: &mut impl Write) -> Result<(), Error> {
        match self {
            Cipher::Encrypt(k) => {
                let key = hill::Key::new(k.a, k.b, k.c, k.d).unwrap();
                let ct = key.encrypt(input);
                writeln!(output, "{}", std::str::from_utf8(&ct)?)?;
            }
            Cipher::Decrypt(k) => {
                let key = hill::Key::new(k.a, k.b, k.c, k.d).unwrap();
                let pt = key.decrypt(input);
                writeln!(output, "{}", std::str::from_utf8(&pt)?)?;
            }
            Cipher::Crack => {
                let key = hill::crack(input);
                let pt = key.decrypt(input);
                writeln!(output, "{:?}\n{}", key, std::str::from_utf8(&pt)?)?;
            }
        }
        Ok(())
    }
}
