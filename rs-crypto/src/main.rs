//! # RS Crypto
//!
//! RS Crypto is a command line interface for
//! interacting with numerous encryption methods
//! using Lib Crypto. The project supports encryption,
//! decryption, and cracking of multiple ciphers.
#![warn(clippy::all)]

pub mod cli;

use self::cli::Opt;
use failure::Error;
use std::fs::File;
use std::io::{stdin, stdout, BufReader, BufWriter, Read, Write};
use structopt::StructOpt;

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    // Create the input and output buffers.
    let ibuff: Box<Read> = match opt.input {
        Some(p) => Box::new(BufReader::new(File::open(p)?)),
        None => Box::new(BufReader::new(stdin())),
    };
    let mut obuff: Box<Write> = match opt.output {
        Some(p) => Box::new(BufWriter::new(File::create(p)?)),
        None => Box::new(BufWriter::new(stdout())),
    };

    // Read all the bytes from the input file.
    let ct: Vec<_> = ibuff.bytes().collect::<Result<_, _>>()?;

    opt.system.run(&ct, &mut obuff)?;

    Ok(())
}
