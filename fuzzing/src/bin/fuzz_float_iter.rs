use honggfuzz::fuzz;
use lib_crypto::utils::FloatIterExt;

fn main() {
    loop {
        fuzz!(|data: Vec<f64>| {
            data.iter().cloned().float_max();
            data.iter().cloned().float_min();
        });
    }
}
