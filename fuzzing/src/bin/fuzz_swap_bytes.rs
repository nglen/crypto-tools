use honggfuzz::fuzz;

fn main() {
    loop {
        fuzz!(|data: &[u8]| {
            if data.len() >= 2 {
                let mut bytes = (&data[2..]).to_vec();
                swap_bytes(&mut bytes, (data[0], data[1]));
            }
        });
    }
}
