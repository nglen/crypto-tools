use honggfuzz::fuzz;
use lib_crypto::utils::mutate;

fn main() {
    loop {
        fuzz!(|data: &[u8]| {
            mutate(data);
        });
    }
}
